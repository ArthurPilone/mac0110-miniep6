Escreva, neste arquivo, sua resposta para a pergunta da parte 2.2 do MiniEP6. Descreva brevemente o que você observou ao executar mostra_n(20). Não precisa entrar em detalhes! Basta 2 ou 3 linhas.
Resposta:
É possível notar que, para cada n que a função analisa, os n ímpares que somados juntos dão n^3 são, 
sempre, os n mais próximos de n^2. É possível entender isto ao percebemos que a média desses 
elementos sempre corresponde a n^2, então temos a soma deles como n*(n^2) = n^3.