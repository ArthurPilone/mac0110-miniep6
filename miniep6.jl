# Parte 2.0 funções auxiliares:
#Alternativa 1: "Na força bruta"
function consecutivoNBruto(n,i)
    j = 1
    achei = false
    while(!achei)
        soma = 0
        k = 0
        while(k < n)
            soma+= j + 2*k
            k+=1
        end
        if(soma == n^3)
            return (j+(i-1)*2)
            achei = true
        end
        j+=2
    end
end
#Alternativa 2: "Utilizando a 'sacada' de n^2"
function consecutivoN(n,i)
    nquad = n^2
    if(nquad%2 == 0)
        return ((nquad-1)-(n-2)) + 2(i-1)
    else
        return (nquad-(n-1)) + 2(i-1)
    end
end
# Parte 2.1 - Escreva abaixo sua função impares_consecutivos(n)
function impares_consecutivos(n)
    return consecutivoN(n,1)
end

# Parte 2.2 - Escreva abaixo suas funções imprime_impares_consecutivos(m) e mostra_n(n)

function imprime_impares_consecutivos(m)
    print(m," ",m^3," ")
    for i in 1:m
        print(consecutivoN(m,i), " ")
    end
    println(" ")
end

function mostra_n(n)
    for j in 1:n
        imprime_impares_consecutivos(j)
    end
end
mostra_n(20)
# Testes automatizados - segue os testes para a parte 2.1. Não há testes para a parte 2.2.

function test()
    if impares_consecutivos(1) != 1
        print("Sua função retornou o valor errado para n = 1")
    end
    if impares_consecutivos(2) != 3
        print("Sua função retornou o valor errado para n = 2")
    end
    if impares_consecutivos(7) != 43
        print("Sua função retornou o valor errado para n = 7")
    end
    if impares_consecutivos(14) != 183
        print("Sua função retornou o valor errado para n = 14")
    end
    if impares_consecutivos(21) != 421
        print("Sua função retornou o valor errado para n = 21")
    end
end

# Para rodar os testes, certifique-se de que suas funções estão com os nomes corretos! Em seguida, descomente a linha abaixo:
 test()
